# Release Notes

## Unreleased

## 0.0.10
 * [Refactor] Обновлено тестирование и инструкции.
 * [New] В интерфейс модели, обрабатывающей каталог добавлен метод `getAnswerDetail()`, который должен возвращать детализированный ответ о текущем шаге. 

## 0.0.9
 * [Bug-fix] В контроллер добавлено отлавливание исключений распаковки файлов.
 * [Refactor] В read.me изменена инструкция по тестам.
 * [Bug-fix] Алиас конфига.

## 0.0.8
 * В файле конфигурации добавлены параметры для указания имени пользователя и пароля для тестов. Если пользователь не существует - создаётся, но НЕ УДАЛЯЕТСЯ.
 * Алиас файла конфигурации изменен с `protocol` на `la1CProtocol`.
